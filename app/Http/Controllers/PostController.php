<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

// access the authenticated user via auth facade
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function createPost()
    {
        return view('posts.create');
    }

    public function storePost(Request $request){

        if (Auth::user()){

            // instanciate a new post from the post model
            $post = new Post();

            // define the properties of the post object using the request data
            
            $post->title = $request->title;
            $post->content = $request->content;
            $post->user_id = auth()->id();

            // save this post object in the database
            $post->save();
            return redirect('post.index');
        } 
        else {
            return redirect('login');
        }
    }

    // action that will return the view showing all the blog posts
    public function showPost(){
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    // action for showing only the post authored by the authenticated user
    public function myPosts(){
        if (Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index', compact('posts'));
        } else
        return redirect('login');
    }

    // action that will return the view showing specific posts using the url parameter id to query for the database entry to be shown
    
    public function showPostById($id)
    {
        $post = Post::find($id);
        return view('posts.show', compact('post'));
    }

    // action that will return the view showing specific posts using the url parameter id to query for the database entry to be shown

    public function editPost($id){
        $post = Post::find($id);

        if(Auth::user())
            if(Auth::user()->id == $post->user_id)
                return view('posts.edit', compact('post'));
            else
                return redirect('post.index');
        else
            return redirect('login');

    }

    public function updatePost($id, Request $request){

        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

         Post::find($id)->update($request->all());
         $posts = Post::all();
        return view('posts.index', compact('posts'));
    }
}