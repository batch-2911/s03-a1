@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>All Posts</h1>
        @if (count($posts) > 0)
            @foreach ($posts as $post)
                <div class="card text-center">
                    <div class="card-body">
                        <h4> <a href="/post/{{ $post->id }}">{{ $post->title}}</a></h4>
                        <p>Author {{ $post->user->name}}</p>
                        <p class="card-subtitle mb-3 text-muted">Created at: {{ $post->created_at }}</p>
                    </div>

                    @if (Auth::user() && Auth::user()->id === $post->user_id)
                        <div class="card-footer">
                           <form method="POST" action="{{ route('posts-delete', $post->id) }}">
                                @method('DELETE')
                                @csrf
                                <a href="{{ route('post.edit', $post->id) }}" class="btn btn-primary"> Edit Post</a>
                                <button type="submit" class="btn btn-danger">Delete Post</button>
                           </form>
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            <div class="card text-center">
                <div class="card-body">
                    <h4>No Posts</h4>
                    <a href="/posts/create" class="btn btn-info">Create Post</a>
                </div>
            </div>
        @endif
    </div>
@endsection